This repository contains my website. It's my way of learning git and web design in one project that's publicly available (thanks GitLab).

I develop on the `dev` branch and merge changes I want to publish into the `pub` branch. For what it's worth, I use vanilla HTML and CSS. I like Sass and Jade, but they felt unnecessary for such a small web project.

Disclaimers:
* Everything's MIT-licensed
* Plagiarism is illegal
* I'm not a lawyer
* Use at own risk

Resources:
* Mozilla Developer Network
* git-scm.com
* git man pages
* GitHub/GitLab help
