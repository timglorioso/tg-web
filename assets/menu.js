var button = document.querySelector('button');
var nav = document.querySelector('nav');
var navActive = false;
var mouseEnabled = false;

function modifyProperties() {
   if (navActive) {
      button.style.color = '#333';
      button.style.backgroundColor = 'transparent';
      nav.style.display = 'none';
      navActive = false;
   } else {
      button.style.color = '#fff';
      button.style.backgroundColor = '#333';
      nav.style.display = 'inline-block';
      navActive = true;
   }
}

function clicked(event) {
   modifyProperties();
}

function touched(event) {
   modifyProperties();
   if (mouseEnabled) {
      button.removeEventListener('mousedown', clicked, false);
      mouseEnabled = false;
   }
}

// button.addEventListener('touchend', touched, false);
button.addEventListener('mousedown', clicked, false);
// mouseEnabled = true;
